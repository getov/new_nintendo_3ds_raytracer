#ifndef _GEOMETRY_H_
#define _GEOMETRY_H_

#include "Vector.h"
#include "Matrix.h"
#include "Color.h"

struct IntersectionData
{
	Vector intersectionPoint;
	Vector intersectionNormal;
	double distance;

	//temp
	Color color;

	Vector dNdx, dNdy;
	double u, v;
};

class Geometry
{
public:
	virtual ~Geometry() {}

	virtual bool intersect(const Ray& ray, IntersectionData& data) = 0;
};

class Node
{
public:
	Geometry* geom;
	//Shader* shader;
	Transform transform;

	//temp
	Color color;

	Node() {}
	Node(Geometry* g, const Color col/*, Shader* s*/) 
	{ 
		geom = g;/* shader = s;*/ 
		color = col;
	}

	bool intersect(const Ray& ray, IntersectionData& data);
};

class Sphere : public Geometry
{
public:
	Vector center;
	double radius;

	Sphere(const Vector& c, double r);

	bool intersect(const Ray& ray, IntersectionData& info);
};

class Plane : public Geometry 
{
	double y; //!< y-intercept. The plane is parallel to XZ, the y-intercept is at this value
	double limit;
public:
	Plane(double _y = 0, double _limit = 1e99) 
	{ 
		y = _y; 
		limit = _limit; 
	}

	bool intersect(const Ray& ray, IntersectionData& data);
};

#endif