#ifndef __CAMERA_H__
#define __CAMERA_H__

#include "Vector.h"

class Camera
{
	// these internal vectors describe three of the ends of the imaginary
	// ray shooting screen
	Vector upLeft;
	Vector upRight;
	Vector downLeft;
	Vector frontDir;
	Vector rightDir;
	Vector upDir;
public:
	Vector pos; //!< position of the camera in 3D.
	double yaw; //!< Yaw angle in degrees (rot. around the Y axis, meaningful values: [0..360])
	double pitch; //!< Pitch angle in degrees (rot. around the X axis, meaningful values: [-90..90])
	double roll; //!< Roll angle in degrees (rot. around the Z axis, meaningful values: [-180..180])
	double fov; //!< The Field of view in degrees (meaningful values: [3..160])
	double aspect; //!< The aspect ratio of the camera frame. Should usually be frameWidth/frameHeight,
	double focalPlaneDist;
	double fNumber;

	Camera() {}

	// from SceneElement:
	void beginFrame(); //!< must be called before each frame. Computes the corner variables, needed for getScreenRay()


	/// generates a screen ray through a pixel (x, y - screen coordinates, not necessarily integer).
	/// if the camera parameter is present - offset the rays' start to the left or to the right,
	/// for use in stereoscopic rendering
	Ray getScreenRay(double x, double y);
};

#endif // __CAMERA_H__
