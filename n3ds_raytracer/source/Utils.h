#pragma once

#include <math.h>

const double PI = 3.141592653589793238462643383;

extern unsigned char RGB_COMPRESS_CACHE[4097];
extern unsigned char SRGB_COMPRESS_CACHE[4097];

inline int nearestInt(float x)
{
	return (int)floor(x + 0.5f);
}

inline unsigned convertTo8bit(float x)
{
	if (x < 0) x = 0;
	if (x > 1) x = 1;
	return nearestInt(x * 255.0f);
}

inline unsigned convertTo8bit_sRGB(float x)
{
	const float a = 0.055f;
	if (x <= 0)
	{
		return 0;
	}
	if (x >= 1)
	{
		return 255;
	}

	// sRGB transform:
	if (x <= 0.0031308f)
	{
		x = x * 12.02f;
	}
	else
	{
		x = (1.0f + a) * powf(x, 1.0f / 2.4f) - a;
	}

	return nearestInt(x * 255.0f);
}

inline double toRadians(double angle)
{
	return angle / 180.0 * PI;
}

inline double toDegrees(double angle_rad)
{
	return angle_rad / PI * 180.0;
}

inline unsigned convertTo8bit_RGB_cached(float x)
{
	if (x <= 0) return 0;
	if (x >= 1) return 255;
	return RGB_COMPRESS_CACHE[int(x * 4096.0f)];
}

inline unsigned convertTo8bit_sRGB_cached(float x)
{
	if (x <= 0) return 0;
	if (x >= 1) return 255;
	return SRGB_COMPRESS_CACHE[int(x * 4096.0f)];
}

inline void precomputeColorCache()
{
	for (int i = 0; i <= 4096; i++)
	{
		RGB_COMPRESS_CACHE[i] = (unsigned char)convertTo8bit(i / 4096.0f);
	}

	for (int i = 0; i <= 4096; i++)
	{
		SRGB_COMPRESS_CACHE[i] = (unsigned char)convertTo8bit_sRGB(i / 4096.0f);
	}
}