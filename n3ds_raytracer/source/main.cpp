#include <3ds.h>
#include <string.h>
#include <stdio.h>
#include <time.h>
#include "Geometry.h"
#include "Matrix.h"
#include "Color.h"
#include "Camera.h"
#include <algorithm>
using namespace std;

const int BUCKET_SIZE = 20; // 20x20
const int TOTAL_BUCKETS = 240; // 20 in width and 12 in height
const int TOP_SCREEN_HEIGHT = 240;
const int TOP_SCREEN_WIDTH = 400;
const int FRAMEBUFFER_SIZE = 240 * 400 * 3;

const int NUM_NODES = 4;
Node sceneElements[NUM_NODES];

bool testVisibility(const Vector& from, const Vector& to)
{
	Ray ray;
	ray.start = from;
	ray.dir = to - from;
	ray.dir.normalize();
	ray.flags |= RF_SHADOW;

	IntersectionData temp;
	temp.distance = (to - from).length();

	// if there's any obstacle between from and to, the points aren't visible.
	// we can stop at the first such object, since we don't care about the distance.
	for (int i = 0; i < NUM_NODES; i++)
	{
		if (sceneElements[i].intersect(ray, temp))
			return false;
	}

	return true;
}

Color shade(const Ray& ray, const IntersectionData& data)
{
	// turn the normal vector towards us (if needed):
	Vector N = faceforward(ray.dir, data.intersectionNormal);

	// fetch the material color. This is ether the solid color, or a color
	// from the texture, if it's set up.
	Color diffuseColor = data.color;
	Color lightColor(1.0, 1.0, 1.0);
	float lightPower = 800000.f;
	lightColor = lightColor * lightPower;

	Vector lightPos(-90.0, 700, 350.0);

	Color lightContrib(0.2, 0.2, 0.2);

	Color avgColor(0, 0, 0);

	if (lightColor.intensity() != 0 && testVisibility(data.intersectionPoint + N * 1e-6, lightPos)) 
	{
		Vector lightDir = lightPos - data.intersectionPoint;
		lightDir.normalize();

		// get the Lambertian cosine of the angle between the geometry's normal and
		// the direction to the light. This will scale the lighting:
		double cosTheta = dot(lightDir, N);
		if (cosTheta > 0)
		{
			avgColor = lightColor / (data.intersectionPoint - lightPos).lengthSqr() * cosTheta;
		}
	}

	lightContrib += avgColor;

	return diffuseColor * lightContrib;
}

Color raytrace(const Ray& ray)
{
	IntersectionData data;
	Node* closestNode=NULL;

	data.distance = 1e99;

	// find closest intersection point:
	for (int i = 0; i < NUM_NODES; i++)
	{
		if (sceneElements[i].intersect(ray, data))
		{
			closestNode = &sceneElements[i];
		}
	}

	// no intersection
	if (!closestNode) {
		return Color(1, 1, 1);
	}

	data.color = closestNode->color;

	// use the shader of the closest node to shade the intersection:
	return shade(ray, data);
}


struct Rect {
	int x0, y0, x1, y1, w, h;
	Rect() {}
	Rect(int _x0, int _y0, int _x1, int _y1)
	{
		x0 = _x0, y0 = _y0, x1 = _x1, y1 = _y1;
		h = y1 - y0;
		w = x1 - x0;
	}
	void clip(int maxX, int maxY); // clips the rectangle against image size
};

void Rect::clip(int W, int H)
{
	x1 = min(x1, W);
	y1 = min(y1, H);
	w = max(0, x1 - x0);
	h = max(0, y1 - y0);
}

Rect bucketList[TOTAL_BUCKETS];
void fillBucketsList()
{
	int W = TOP_SCREEN_WIDTH;
	int H = TOP_SCREEN_HEIGHT;
	int BW = (W - 1) / BUCKET_SIZE + 1;
	int BH = (H - 1) / BUCKET_SIZE + 1;

	int index = 0;
	for (int y = 0; y < BH; y++) 
	{
		if (y % 2 == 0)
		{
			for (int x = 0; x < BW; x++)
			{
				bucketList[index] = Rect(x * BUCKET_SIZE, y * BUCKET_SIZE, (x + 1) * BUCKET_SIZE, (y + 1) * BUCKET_SIZE);
				++index;
			}
		}
		else
		{
			for (int x = BW - 1; x >= 0; x--)
			{
				bucketList[index] = Rect(x * BUCKET_SIZE, y * BUCKET_SIZE, (x + 1) * BUCKET_SIZE, (y + 1) * BUCKET_SIZE);
				++index;
			}
		}

	}

	//for (int i = 0; i < TOTAL_BUCKETS; i++)
	//{
	//	bucketList[i].clip(W, H);
	//}
}

void drawBracketOne(u8* fb, Rect& r, int x, int y)
{
	int X = r.x0 + x;
	int Y = r.y0 + y;
	const int buffer_index = 3 * (X*TOP_SCREEN_HEIGHT + Y);

	fb[buffer_index] = 255;
	fb[buffer_index+1] = 255;
	fb[buffer_index+2] = 255;
}

void drawBracket(u8* fb, Rect& r, int x, int y)
{
	drawBracketOne(fb, r, x, y);
	drawBracketOne(fb, r, y, x);
	drawBracketOne(fb, r, r.w - 1 - x, y);
	drawBracketOne(fb, r, r.w - 1 - y, x);
	drawBracketOne(fb, r, x, r.h - 1 - y);
	drawBracketOne(fb, r, y, r.h - 1 - x);
	drawBracketOne(fb, r, r.w - 1 - x, r.h - 1 - y);
	drawBracketOne(fb, r, r.w - 1 - y, r.h - 1 - x);
}

void markRegion(u8* fb, Rect& r)
{
	const int L = 4;

	for (int i = 1; i <= L; i++)
	{
		drawBracket(fb, r, i, 0);
	}
	drawBracket(fb, r, 1, 1);
	drawBracket(fb, r, L + 1, 1);
	for (int i = 0; i <= L; i++)
	{
		drawBracket(fb, r, i, 2);
	}
	for (int i = 2; i <= L; i++)
	{
		drawBracket(fb, r, i, 1);
	}
}

void renderBuckets(u8* fb, Camera& camera)
{
	for (int i = 0; i < TOTAL_BUCKETS; ++i)
	{
		Rect& r = bucketList[i];

		markRegion(fb, r);
		gfxFlushBuffers();
		gfxSwapBuffers();

		for (int dx = 0; dx < r.w; ++dx)
		{
			for (int dy = 0; dy < r.h; ++dy)
			{
				int x = r.x0 + dx;
				int y = r.y0 + dy;
				const int buffer_index = 3 * (x*TOP_SCREEN_HEIGHT + y);

				Ray screenRay = camera.getScreenRay(x, y);
				Color result = raytrace(screenRay);
				fb[buffer_index] = convertTo8bit_RGB_cached(result.b);
				fb[buffer_index + 1] = convertTo8bit_RGB_cached(result.g);
				fb[buffer_index + 2] = convertTo8bit_RGB_cached(result.r);

			}
		}
		gfxFlushBuffers();
		gfxSwapBuffers();
	}
}

void renderPixels(u8* fb, Camera& camera)
{
	for (int x = 0; x < TOP_SCREEN_WIDTH; ++x)
	{
		for (int y = 0; y < TOP_SCREEN_HEIGHT; ++y)
		{
			const int buffer_index = 3 * (TOP_SCREEN_HEIGHT*x + y);
	
			Ray screenRay = camera.getScreenRay(x, y);
			Color result = raytrace(screenRay);
	
			fb[buffer_index]   = convertTo8bit_RGB_cached(result.b);
			fb[buffer_index+1] = convertTo8bit_RGB_cached(result.g);
			fb[buffer_index+2] = convertTo8bit_RGB_cached(result.r);
		}
	}
}


// TESTING THREADS ==============================================
#define NUMTHREADS 3
#define STACKSIZE (4 * 1024)
volatile bool runThreads = true;

struct mtArgs
{
	u8* fb;
	Camera* camera;
	int threadIndex;

	mtArgs(u8* frameBuffer, Camera* cam)
	{
		fb = frameBuffer;
		camera = cam;
		threadIndex = 0;
	}
};

void renderBucketsThreaded(void* args)
{
	mtArgs* argData = static_cast<mtArgs*>(args);
	u8* fb = argData->fb;
	Camera* camera = argData->camera;
	int threadIndex = argData->threadIndex;
	int start = threadIndex * 80;
	int end = (threadIndex + 1) * 80; // TOTAL_BUCKETS / 3 threads = 80

	if (end >= TOTAL_BUCKETS)
	{
		end = TOTAL_BUCKETS;
	}

	for (int i = start; i < end; ++i)
	{
		Rect& r = bucketList[i];

		markRegion(fb, r);
		gfxFlushBuffers();
		gfxSwapBuffers();

		for (int dx = 0; dx < r.w; ++dx)
		{
			for (int dy = 0; dy < r.h; ++dy)
			{
				int x = r.x0 + dx;
				int y = r.y0 + dy;
				const int buffer_index = 3 * (x*TOP_SCREEN_HEIGHT + y);

				Ray screenRay = camera->getScreenRay(x, y);
				Color result = raytrace(screenRay);
				fb[buffer_index] = convertTo8bit_RGB_cached(result.b);
				fb[buffer_index + 1] = convertTo8bit_RGB_cached(result.g);
				fb[buffer_index + 2] = convertTo8bit_RGB_cached(result.r);

			}
		}
		gfxFlushBuffers();
		gfxSwapBuffers();
	}
}

// TESTING THREADS ==============================================

void initScene()
{
	// TODO
}

int main()
{
	// Initialize graphics
	gfxInitDefault();

	gfxSetDoubleBuffering(GFX_TOP, true);

	// Init bottom screen console
	consoleInit(GFX_BOTTOM, NULL);

	precomputeColorCache();
	fillBucketsList();

	Camera camera;
	camera.pos = Vector(0.0, 165.0, 0.0);
	camera.pitch = -30.0;
	camera.yaw = 0.0;
	camera.roll = 0.0;
	camera.fov = 90.0;

	camera.beginFrame();

	// INIT SCENE
	Sphere orb(Vector(60, 50, 220), 60.0);
	Plane plane(-0.01);
	sceneElements[0] = Node(&orb, Color(1.0, 0.0, 0.0));
	sceneElements[1] = Node(&plane, Color(0.1, 0.8, 0.0));
	Sphere orb1(Vector(-60, 50, 220), 60.0);
	sceneElements[2] = Node(&orb1, Color(0.0, 1.0, 1.0));
	Sphere orb2(Vector(0, 20, 150), 30.0);
	sceneElements[3] = Node(&orb2, Color(0.5, 0.5, 0.5));
	// INIT SCENE

	u8* fb = gfxGetFramebuffer(GFX_TOP, GFX_LEFT, NULL, NULL);
	memset(fb, 0, FRAMEBUFFER_SIZE);
	
	time_t unixTime = time(NULL);
	struct tm* timeStruct = gmtime((const time_t *)&unixTime);
	float startTime = timeStruct->tm_sec;

#define THREADED
#ifdef THREADED
	mtArgs mtArgs(fb, &camera);
	Thread threads[NUMTHREADS];
	s32 prio = 0;
	svcGetThreadPriority(&prio, CUR_THREAD_HANDLE);
	printf("Main thread prio: 0x%lx\n", prio);

	for (int i = 0; i < NUMTHREADS; i++)
	{
		mtArgs.threadIndex = i;
		// The priority of these child threads must be higher (aka the value is lower) than that
		// of the main thread, otherwise there is thread starvation due to stdio being locked.
		threads[i] = threadCreate(renderBucketsThreaded, (void*)(&mtArgs), STACKSIZE, prio - 1, -2, false);
		printf("created thread %d: %p\n", i, threads[i]);
	}

	for (int i = 0; i < NUMTHREADS; i++)
	{
		threadJoin(threads[i], U64_MAX);
		threadFree(threads[i]);
	}
	time_t unixTime2 = time(NULL);
	struct tm* timeStruct2 = gmtime((const time_t *)&unixTime2);
	float endTime = timeStruct2->tm_sec;
	printf("render time: %f sec\n", (endTime - startTime));
#else
	renderBuckets(fb, camera);
	time_t unixTime2 = time(NULL);
	struct tm* timeStruct2 = gmtime((const time_t *)&unixTime2);
	float endTime = timeStruct2->tm_sec;
	printf("render time: %f sec\n", (endTime - startTime));
#endif

	// old way
	//renderBuckets(fb, camera);
	//gfxFlushBuffers();
	//gfxSwapBuffers();

#ifdef THREADED
	//gfxFlushBuffers();
	//gfxSwapBuffers();

#endif

	bool done = false;
	while (aptMainLoop())
	{
		gspWaitForVBlank();
		hidScanInput();

		u32 kDown = hidKeysDown();
		if (kDown & KEY_START)
		{
			break; // break in order to return to hbmenu
		}

		if (!done) 
		{
			//renderPixels(fb, camera);
			gfxFlushBuffers();
			gfxSwapBuffers();
			done = true;
		}
	}

	gfxExit();
	return 0;
}
