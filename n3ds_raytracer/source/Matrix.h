#ifndef __MATRIX_H__
#define __MATRIX_H__

#include "Vector.h"
#include "Utils.h"

struct Matrix 
{
	double m[3][3];
	Matrix() {}
	Matrix(double diagonalElement)
	{
		for (int i = 0; i < 3; i++)
		{
			for (int j = 0; j < 3; j++)
			{
				if (i == j)
				{
					m[i][j] = diagonalElement;
				}
				else
				{
					m[i][j] = 0.0;
				}
			}
		}
	}
};

inline Vector operator * (const Vector& v, const Matrix& m)
{
	return Vector(
		v.x * m.m[0][0] + v.y * m.m[1][0] + v.z * m.m[2][0],
		v.x * m.m[0][1] + v.y * m.m[1][1] + v.z * m.m[2][1],
		v.x * m.m[0][2] + v.y * m.m[1][2] + v.z * m.m[2][2]
		);
}

inline void operator *= (Vector& v, const Matrix& a) 
{ 
	v = v*a; 
}

Matrix operator * (const Matrix& a, const Matrix& b); // matrix multiplication; result = a*b
Matrix inverseMatrix(const Matrix& a); // finds the inverse of a matrix (assuming it exists)
Matrix transpose(const Matrix& a); // finds the transposed matrix
double determinant(const Matrix& a); // finds the determinant of a matrix

Matrix rotationAroundX(double angle); // returns a rotation matrix around the X axis; the angle is in radians
Matrix rotationAroundY(double angle); // same as above, but rotate around Y
Matrix rotationAroundZ(double angle); // same as above, but rotate around Z

class Transform 
{
public:
	Transform() 
	{ 
		reset(); 
	}

	void reset() 
	{
		transform = Matrix(1);
		inverseTransform = inverseMatrix(transform);
		transposedInverse = Matrix(1);
		offset.makeZero();
	}

	void scale(double X, double Y, double Z) 
	{
		Matrix scaling(X);
		scaling.m[1][1] = Y;
		scaling.m[2][2] = Z;

		transform = transform * scaling;
		inverseTransform = inverseMatrix(transform);
		transposedInverse = transpose(inverseTransform);
	}

	void rotate(double yaw, double pitch, double roll) 
	{
		transform = transform *
			rotationAroundX(toRadians(pitch)) *
			rotationAroundY(toRadians(yaw)) *
			rotationAroundZ(toRadians(roll));
		inverseTransform = inverseMatrix(transform);
		transposedInverse = transpose(inverseTransform);
	}

	void translate(const Vector& V) 
	{
		offset = V;
	}

	Vector point(Vector P) const 
	{
		P = P * transform;
		P = P + offset;

		return P;
	}

	Vector undoPoint(Vector P) const 
	{
		P = P - offset;
		P = P * inverseTransform;

		return P;
	}

	Vector direction(const Vector& dir) const 
	{
		return dir * transform;
	}

	Vector normal(const Vector& dir) const 
	{
		return dir * transposedInverse;
	}

	Vector undoDirection(const Vector& dir) const 
	{
		return dir * inverseTransform;
	}

	Ray ray(const Ray& inputRay) const 
	{
		Ray result = inputRay;
		result.start = point(inputRay.start);
		result.dir = direction(inputRay.dir);
		return result;
	}

	Ray undoRay(const Ray& inputRay) const 
	{
		Ray result = inputRay;
		result.start = undoPoint(inputRay.start);
		result.dir = undoDirection(inputRay.dir);
		return result;
	}

private:
	Matrix transform;
	Matrix inverseTransform;
	Matrix transposedInverse;
	Vector offset;
};

#endif // __MATRIX_H__
