#include "Matrix.h"
#include "Geometry.h"
#include "Utils.h"

bool Node::intersect(const Ray& ray, IntersectionData& data)
{
	// world space -> object's canonic space
	Ray rayCanonic;
	rayCanonic.start = transform.undoPoint(ray.start);
	rayCanonic.dir = transform.undoDirection(ray.dir);
	rayCanonic.flags = ray.flags;
	rayCanonic.depth = ray.depth;

	// save the old "best dist", in case we need to restore it later
	double oldDist = data.distance; // *(1)
	double rayDirLength = rayCanonic.dir.length();
	data.distance *= rayDirLength;  // (2)
	rayCanonic.dir.normalize();        // (3)
	if (!geom->intersect(rayCanonic, data)) {
		data.distance = oldDist;    // (4)
		return false;
	}
	// The intersection found is in object space, convert to world space:
	data.intersectionNormal = normalize(transform.normal(data.intersectionNormal));
	data.dNdx = normalize(transform.direction(data.dNdx));
	data.dNdy = normalize(transform.direction(data.dNdy));
	data.intersectionPoint = transform.point(data.intersectionPoint);
	data.distance /= rayDirLength;  // (5)
	return true;
}

Sphere::Sphere(const Vector& c, double r)
	: center(c)
	, radius(r)
{
}

bool Sphere::intersect(const Ray& ray, IntersectionData& info)
{
	// compute the sphere intersection using a quadratic equation:
	Vector H = ray.start - center;
	double A = ray.dir.lengthSqr();
	double B = 2 * dot(H, ray.dir);
	double C = H.lengthSqr() - radius*radius;
	double Dscr = B*B - 4 * A*C;
	if (Dscr < 0) return false; // no solutions to the quadratic equation - then we don't have an intersection.
	double x1, x2;
	x1 = (-B + sqrt(Dscr)) / (2 * A);
	x2 = (-B - sqrt(Dscr)) / (2 * A);
	double sol = x2; // get the closer of the two solutions...
	if (sol < 0) sol = x1; // ... but if it's behind us, opt for the other one
	if (sol < 0) return false; // ... still behind? Then the whole sphere is behind us - no intersection.

							   // if the distance to the intersection doesn't optimize our current distance, bail out:
	if (sol > info.distance) return false;

	info.distance = sol;
	info.intersectionPoint = ray.start + ray.dir * sol;
	info.intersectionNormal = info.intersectionPoint - center; // generate the normal by getting the direction from the center to the ip
	info.intersectionNormal.normalize();


	//double angle = atan2(info.intersectionPoint.z - center.z, info.intersectionPoint.x - center.x);
	//info.u = (PI + angle) / (2 * PI);
	//info.v = 1.0 - (PI / 2 + asin((info.intersectionPoint.y - center.y) / radius)) / PI;
	//info.dNdx = Vector(cos(angle + PI / 2), 0, sin(angle + PI / 2));
	//info.dNdy = info.dNdx ^ info.intersectionNormal;

	return true;
}

bool Plane::intersect(const Ray& ray, IntersectionData& data)
{
	// intersect a ray with a XZ plane:
	// if the ray is pointing to the horizon, or "up", but the plane is below us,
	// of if the ray is pointing down, and the plane is above us, we have no intersection
	if ((ray.start.y > y && ray.dir.y > -1e-9) || (ray.start.y < y && ray.dir.y < 1e-9))
		return false;
	else {
		double yDiff = ray.dir.y;
		double wantYDiff = ray.start.y - this->y;
		double mult = wantYDiff / -yDiff;

		// if the distance to the intersection (mult) doesn't optimize our current distance, bail out:
		if (mult > data.distance) return false;

		Vector p = ray.start + ray.dir * mult;
		if (fabs(p.x) > limit || fabs(p.z) > limit) return false;

		// calculate intersection:
		data.intersectionPoint = p;
		data.distance = mult;
		data.intersectionNormal = Vector(0, 1, 0);
		data.dNdx = Vector(1, 0, 0);
		data.dNdy = Vector(0, 0, 1);
		data.u = data.intersectionPoint.x;
		data.v = data.intersectionPoint.z;
		//data.g = this;
		return true;
	}
}