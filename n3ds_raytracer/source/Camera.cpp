#include "Camera.h"
#include "Matrix.h"
#include "Utils.h"

void Camera::beginFrame(void)
{
	aspect = 400.0 / 240.0;
	double x = -aspect;
	double y = 1;

	Vector corner = Vector(x, y, 1);
	Vector center = Vector(0, 0, 1);

	double lenXY = (corner - center).length();
	double wantedLength = tan(toRadians(fov / 2));

	double scaling = wantedLength / lenXY;

	x *= scaling;
	y *= scaling;

	this->upLeft = Vector(-x, -y, 1);
	this->upRight = Vector(x, -y, 1);
	this->downLeft = Vector(-x, y, 1);

	Matrix rotation = rotationAroundZ(toRadians(roll))
		* rotationAroundX(toRadians(pitch))
		* rotationAroundY(toRadians(yaw));
	upLeft *= rotation;
	upRight *= rotation;
	downLeft *= rotation;
	rightDir = Vector(1, 0, 0) * rotation;
	upDir = Vector(0, 1, 0) * rotation;
	frontDir = Vector(0, 0, 1) * rotation;

	upLeft += pos;
	upRight += pos;
	downLeft += pos;
}

Ray Camera::getScreenRay(double x, double y)
{
	Ray result; // A, B -     C = A + (B - A) * x
	result.start = this->pos;
	Vector target = upLeft +
		(upRight - upLeft) * (x / 400.0) +
		(downLeft - upLeft) * (y / 240.0);

	// A - camera; B = target
	result.dir = target - this->pos;

	result.dir.normalize();

	return result;
}
